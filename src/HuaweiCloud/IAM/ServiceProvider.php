<?php
/**
 * Copyright (c) 2018-2022.
 * This file is part of the moonpie production
 *   (c) johnzhang <875010341@qq.com>
 *   This source file is subject to the MIT license that is bundled
 *  with this source code in the file LICENSE.
 */

namespace Moonpie\Macro\HuaweiCloud\IAM;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        if (!isset($app['iam'])) {
            $app['iam'] = function($c){
                return new Client($c);
            };
        }
    }
}