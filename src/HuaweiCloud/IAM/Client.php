<?php
/**
 * Copyright (c) 2018-2019.
 * This file is part of the moonpie production
 * (c) johnzhang <875010341@qq.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Moonpie\Macro\HuaweiCloud\IAM;


use EasyWeChat\Kernel\BaseClient;
use EasyWeChat\Kernel\Contracts\AccessTokenInterface;
use Moonpie\Macro\ByteMiniProgram\Application;

/**
 * Class Client
 * 处理抖音用户缓存相关的接口
 * @package Moonpie\Macro\ByteMiniProgram\Storage
 */
class Client extends BaseClient
{
    protected $baseUri = 'https://iam.myhuaweicloud.com';
    public function __construct(Application $app, AccessTokenInterface $accessToken = null)
    {
        parent::__construct($app, $accessToken);
    }

}