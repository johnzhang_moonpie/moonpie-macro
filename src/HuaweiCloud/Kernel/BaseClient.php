<?php
/**
 * Copyright (c) 2018-2022.
 * This file is part of the moonpie production
 *   (c) johnzhang <875010341@qq.com>
 *   This source file is subject to the MIT license that is bundled
 *  with this source code in the file LICENSE.
 */

namespace Moonpie\Macro\HuaweiCloud\Kernel;
use EasyWeChat\Kernel\BaseClient as BC;


class BaseClient extends BC
{
    public function fetchRealUrl($uriTmpl, $module)
    {
        //$module     = 'moderation';
        $project_id = $this->app->config->get($module.'.project.id');
        $resource   = strtr($uriTmpl, ['{project_id}' => $project_id]);
        $endpoint   = $this->app->config->get($module.'.project.domain');
        if (empty($endpoint)) {
            $endpoint = '';
        } else {
            $endpoint = '.'.$endpoint;
        }

        return strtr(
            'https://{module}{endpoint}.myhuaweicloud.com{resource}',
            [
                '{module}'   => $this->module2endpoint($module),
                '{endpoint}' => $endpoint,
                '{resource}' => $resource,
            ]
        );
    }
    protected function module2endpoint($module)
    {
        switch ($module) {
            case 'nlp':
                return 'nlp-ext';
            default:
                return $module;
        }
    }
}