<?php
/**
 * Copyright (c) 2018-2022.
 * This file is part of the moonpie production
 *   (c) johnzhang <875010341@qq.com>
 *   This source file is subject to the MIT license that is bundled
 *  with this source code in the file LICENSE.
 */

namespace Moonpie\Macro\HuaweiCloud\Moderation;


use Moonpie\Macro\HuaweiCloud\Kernel\SkToken;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        if (!isset($app['moderation'])) {
            $app['moderation'] = function($c){
                $ak = $c->config->get('ak');
                $sk = $c->config->get('sk');
                $access_token = new SkToken($ak, $sk);
                return new Client($c, $access_token);
            };
        }
    }
}