<?php
/**
 * Copyright (c) 2018-2022.
 * This file is part of the moonpie production
 *   (c) johnzhang <875010341@qq.com>
 *   This source file is subject to the MIT license that is bundled
 *  with this source code in the file LICENSE.
 */

namespace Moonpie\Macro\HuaweiCloud\Moderation;



use EasyWeChat\Kernel\Support\Arr;
use Moonpie\Macro\HuaweiCloud\Kernel\BaseClient;

class Client extends BaseClient
{
    public function text($content, $categories = [])
    {
        $data = [
            'items' => [
                ['text' => $content, 'type' => 'content'],
            ],
        ];
        if (!empty($categories)) {
            $data['categories'] = $categories;
        }

        return $this->httpPostJson(
            $this->fetchRealUrl(
                '/v2/{project_id}/moderation/text',
                'moderation'
            ),
            $data
        );
    }


    public function image($options)
    {
        if (empty($options['image']) && empty($options['url'])) {
            throw new \LogicException('请先确定要使用的图片地址或图片内容');
        }
        $json = Arr::only($options,
            [
                'image',
                'url',
                'categories',
                'ad_categories',
                'moderation_rule',
                'threshold',
            ]
        );

        return $this->httpPostJson(
            $this->fetchRealUrl(
                '/v2/{project_id}/moderation/image', 'moderation'
            ),
            $json
        );
    }

    /**
     * 图片批量审核
     */
    public function imageBatch($urls, $options)
    {
        $json         = Arr::only($options, ['categories', 'threshold']);
        $json['urls'] = $urls;

        return $this->httpPostJson(
            $this->fetchRealUrl(
                '/v2/{project_id}/moderation/image/batch',
                'moderation'
            ),
            $json
        );
    }

    /**
     * 图像批量异步审核
     */
    public function imageBatchAsync($urls, $categories = [])
    {
        $json = ['urls' => $urls];
        if (!empty($categories)) {
            $json['categories'] = $categories;
        }

        return $this->httpPostJson(
            $this->fetchRealUrl(
                '/v2/{project_id}/moderation/image/batch/jobs',
                'moderation'
            ),
            $json
        );
    }

    /**
     * 查询具体审核任务情况
     * @param string $jobId
     */
    public function queryJobResult($jobId)
    {
        return $this->httpGet(
            $this->fetchRealUrl('/v2/{project_id}/moderation/image/batch', 'moderation'),
            ['job_id' => $jobId]
        );
    }
    /**
     * 按照状态查询所有的审核任务列表
     */
    public function queryJobs($status)
    {
        return $this->httpGet(
            $this->fetchRealUrl('/v2/{project_id}/moderation/image/batch/jobs', 'moderation'),
            ['status' => $status]
        );
    }
    public function voice($config, $options)
    {
        if (empty($options['data']) && empty($options['url'])) {
            throw new \LogicException('请先确定要使用的短语音地址或内容');
        }
        $json = Arr::only($options,
            [
                'data',
                'url',
                'categories',
            ]
        );
        $options['config'] = $config;

        return $this->httpPostJson(
            $this->fetchRealUrl(
                '/v2/{project_id}/moderation/voice', 'moderation'
            ),
            $json
        );
    }
}