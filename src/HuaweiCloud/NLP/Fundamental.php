<?php
/**
 * Copyright (c) 2018-2022.
 * This file is part of the moonpie production
 *   (c) johnzhang <875010341@qq.com>
 *   This source file is subject to the MIT license that is bundled
 *  with this source code in the file LICENSE.
 */

namespace Moonpie\Macro\HuaweiCloud\NLP;



use EasyWeChat\Kernel\Support\Arr;
use Moonpie\Macro\HuaweiCloud\Kernel\BaseClient;

class Fundamental extends BaseClient
{

    public function segment($word, $options = [])
    {
        $json = Arr::only(
            $options,
            ['pos_switch', 'lang', 'criterion']
        );
        $json['text'] = $word;

        return $this->httpPostJson(
            $this->fetchRealUrl(
                '/v1/{project_id}/nlp-fundamental/segment',
                'nlp'
            ),
            $json
        );
    }
}