<?php
/**
 * Copyright (c) 2018-2022.
 * This file is part of the moonpie production
 *   (c) johnzhang <875010341@qq.com>
 *   This source file is subject to the MIT license that is bundled
 *  with this source code in the file LICENSE.
 */

namespace Moonpie\Macro\HuaweiCloud\NLP;


use EasyWeChat\Kernel\ServiceContainer;
use EasyWeChat\Kernel\Support\Arr;
use Moonpie\Macro\HuaweiCloud\Kernel\AccessToken;
use Moonpie\Macro\HuaweiCloud\Kernel\SkToken;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        if (!isset($app['nlp_basic'])) {
            $app['nlp_basic'] = function ($c) {
                $access_token = $this->buildAccessToken($c, 'nlp');

                return new Fundamental($c, $access_token);
            };
        }
    }

    /**
     * @param $app ServiceContainer
     */
    protected function buildAccessToken($app, $module)
    {
        $method = $app->config->get("{$module}.access_token.type", 'ak');
        switch ($method) {
            case 'token':
                $ima = Arr::only($app->getConfig(), ['id', 'name', 'password', 'domain', 'agency']);
                $project = $app->config->get("{$module}.project", []);
                $access_token = new AccessToken($app, $ima, $project);
                return $access_token;
            case 'ak':
            default:
                $ak = $app->config->get('ak');
                $sk = $app->config->get('sk');

                return new SkToken($ak, $sk);
        }
    }
}