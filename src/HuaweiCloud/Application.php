<?php
/**
 * Copyright (c) 2018-2022.
 * This file is part of the moonpie production
 *   (c) johnzhang <875010341@qq.com>
 *   This source file is subject to the MIT license that is bundled
 *  with this source code in the file LICENSE.
 */

namespace Moonpie\Macro\HuaweiCloud;

use EasyWeChat\Kernel\ServiceContainer;

/**
 * Class Application
 * 华为云相关接口的应用集合
 * $config = [
 *     'ak' => 'test',
 *     'sk' => 'test'
 * 'iam' => [
 *     'method' => 'sk/token/maf_token/delegate',
 *     'username' => '',
 * ]
 *
 * ];
 * @package Moonpie\Macro\HuaweiCloud
 * @property Moderation\Client $moderation
 * @property NLP\Fundamental $nlp_basic
 */
class Application extends ServiceContainer
{
    protected $providers = [
        Moderation\ServiceProvider::class,
        NLP\ServiceProvider::class,
    ];
}