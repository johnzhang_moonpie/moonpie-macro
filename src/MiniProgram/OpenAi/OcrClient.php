<?php

/*
 *  Copyright (c) 2018-2019.
 *  This file is part of the moonpie production
 *  (c) johnzhang <875010341@qq.com>
 *  This source file is subject to the MIT license that is bundled
 *  with this source code in the file LICENSE.
*/

namespace Moonpie\Macro\MiniProgram\OpenAi;

use \EasyWeChat\Kernel\BaseClient;

/**
 * 开发AI相关接口
 * OCR部分
 * @author johnzhang <875010341@qq.com>
 */
class OcrClient extends BaseClient
{
    /**
     * 身份证识别
     * @param string|resource $img
     *
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     *
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function idCardOCR($img)
    {
        if (is_resource($img)) {

            $options = [
                'multipart' => [
                    ['name' => 'img', 'contents' => $img],
                ],
            ];
        } else {
            $options = [
                'form_params' => [
                    'img_url' => $img,
                ],
            ];
        }

        return $this->request(
            '/cv/ocr/idcard',
            'POST',
            $options
        );
    }

    public function businessLicenseOCR($img)
    {
        if (is_resource($img)) {

            $options = [
                'multipart' => [
                    ['name' => 'img', 'contents' => $img],
                ],
            ];
        } else {
            $options = [
                'form_params' => [
                    'img_url' => $img,
                ],
            ];
        }

        return $this->request(
            '/cv/ocr/bizlicense',
            'POST',
            $options
        );
    }
}
